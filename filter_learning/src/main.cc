/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include <iostream>
#include <string>
#include <csignal>
#include "opencv2/opencv.hpp"
#include <boost/shared_ptr.hpp>

#include "cmd_line.hh"
#include "parameters.hh"
#include "dataset.hh"
#include "filter_bank.hh"
#include "optimization.hh"
#include "exceptions.hh"

#include "2D/dataset_SS.hh"
#include "2D/std_filter_bank_SS.hh"
#include "2D/sparseit_SS.hh"

// Function in charge to deal with signals
void handle_signals(int signum) {
  std::cerr << "Terminating signal caught, shutting down the simulation" << std::endl;
  terminating_signal = 1;
}
// Global variable which exposes the presence of terminating signals
sig_atomic_t terminating_signal;

int main(int argc,char *argv[])
{
  // Initialize signal handler for SIGQUIT
  struct sigaction act;
  act.sa_handler = handle_signals;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  sigaction(SIGQUIT,&act,0);

  std::cerr << "+ Parsing command line and, eventually, resuming a previous simulation" << std::endl;
  CmdLine cmd_line(argc,argv);
  std::cerr << "+ Parsing system configuration file" << std::endl;
  Parameters config(cmd_line);

  // Initialize randomizer
  cv::RNG rng(config.get_rand_seed());

  // Loading filter bank configuration
  std::cerr << "Loading filter bank configuration and initializing it" << std::endl;
  boost::shared_ptr< Filter_bank > filters;
  if (config.get_filter_bank_type()=="std_filter_bank_2D_SS") {
	filters = boost::shared_ptr< Filter_bank >(new STD_filter_bank_2D_SS(cmd_line,config,rng));
  }
  else {
	throw ConfigParseException("Unknown filter bank type requested");
  }

  // Load dataset with its configuration
  std::cerr << "Loading dataset" << std::endl;
  boost::shared_ptr< Dataset > dataset;
  if (config.get_dataset_type()=="dataset_2D_SS") {
	dataset = boost::shared_ptr< Dataset >(new Dataset_2D_SS(config,filters,rng));
  }
  if (!dataset) {
	throw ConfigParseException("Unknown dataset type requested");
  }

  // Load optimization algorithm configuration
  std::cerr << "Loading optimization algorithm's configuration" << std::endl;
  boost::shared_ptr< Optimization > optimization;
  if (config.get_opt_algo()=="sparseit_2D_SS") {
	optimization = boost::shared_ptr< Optimization >(new SparseIT_2D_SS(cmd_line,config,filters,dataset));
  }
  if (!optimization) {
	throw ConfigParseException("Unknown optimization algorithm requested");
  }

  std::cerr << "-- Starting optimization loop (press CTRL-\\ to quit) --" << std::endl;
  optimization->optimize_filters();
}
