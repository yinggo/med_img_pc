/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "parameters.hh"

Parameters::Parameters(const CmdLine& cmd_line) {
  po::options_description cfg_file_descr("Configuration file parameters");

  // Configuration file options
  cfg_file_descr.add_options()
	("rand_seed", po::value< unsigned int >(&(m_rand_seed)),
	 "Random number generator's seed")
	("dataset", po::value< std::string >(&(m_dataset_type)),
	 "Dataset")
	("dataset_config_filename", po::value< std::string >(&(m_dataset_config_filename)),
	 "Datset configuration file")
	("filter_bank", po::value< std::string >(&(m_filter_bank_type)),
	 "Filter bank")
	("filter_bank_config_filename", po::value< std::string >(&(m_filter_bank_config_filename)),
	 "Filter bank configuration file")
	("opt_algo", po::value< std::string >(&(m_opt_algo)),
	 "Optimization algorithm")
	("opt_algo_config_filename", po::value< std::string >(&(m_opt_algo_config_filename)),
	 "Optimization algorithm's configuration file")
	("results_directory", po::value< std::string >(&(m_results_directory)),
	 "Results directory");

  // Parse config file
  std::ifstream ifs(cmd_line.get_config_filename().c_str());
  if (!ifs.is_open()) {
	std::string err_msg("Unable to read the system configuration file "+cmd_line.get_config_filename());
	throw ConfigParseException(err_msg.c_str());
  }
  po::variables_map cfg_file_vm;
  po::store(po::parse_config_file(ifs,cfg_file_descr),cfg_file_vm);
  po::notify(cfg_file_vm);
  ifs.close();

  // If requested, clear the simulation directory
  if (cmd_line.clearall()) {
	if (Utils::dir_exists(m_results_directory)) {
	  if (Utils::get_confirmation("clear the previous simulation")) {
		std::cerr << "Cleaning previous simulation's data" << std::endl;
		Utils::delete_dir(m_results_directory);
	  }
	  else {
		throw SimulationException("Wrong confirmation code entered");
	  }
	}
  }

  // Verify if the result directory already exists
  if (Utils::dir_exists(m_results_directory)) {
  	if (!cmd_line.resume() && !cmd_line.clear()) {
	  throw SimulationException("The result directory already exists but no resume or clear flag has been specified");
	}
  }
  else {
	Utils::create_dir(m_results_directory);
  }
}
