/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "utils.hh"

bool Utils::dir_exists(const std::string& dir_name) {
  fs::path path(dir_name);
  if (!fs::exists(path) || !fs::is_directory(path))
	return(false);
  return(true);
}

void Utils::delete_dir(const std::string& dir_name) {
  fs::path path(dir_name);
  if (fs::exists(path) && fs::is_directory(path))
	fs::remove_all(path);
}

bool Utils::file_exists(const std::string& file_name) {
  fs::path path(file_name);
  if (!fs::exists(path) || fs::is_directory(path))
	return(false);
  return(true);
}

void Utils::create_dir(const std::string& dir_name) {
  fs::path path(dir_name);
  if (!fs::create_directories(path)) {
	std::string err_msg("Error encountered while creating the directory: "+dir_name);
	throw PathException(err_msg.c_str());
  }
}

bool Utils::get_confirmation(const std::string& operation_name) {
  std::cerr << "  The requested action (" << operation_name << ") has to be confirmed before executing it." << std::endl;
  srand(time(0));
  unsigned int rand_n = rand()%1000;
  std::cerr << "  Please type the number " << rand_n << " to confirm it: ";
  unsigned int user_n;
  std::cin >> user_n;
  return(rand_n==user_n);
}

void Utils::compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float,float > >& ms_vector) {
  std::vector< cv::Mat >::const_iterator img_it = images.begin();
  cv::Scalar mean, std_dev;
  unsigned int img_n = 0;

  for (;img_it!=images.end();++img_it,++img_n) {
	cv::meanStdDev(*img_it,mean,std_dev);

    if (std_dev[0]<1e-5) {
      std::string err_msg("Insufficient variance in image number "+img_n);
      throw(ProcessingException(err_msg.c_str()));
    }

    ms_vector.push_back(std::pair< float,float >(mean[0],std_dev[0]));
  }
}

void Utils::compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float,float > >& ms_vector,
								const std::vector< cv::Mat >& masks) {
  std::vector< cv::Mat >::const_iterator img_it = images.begin();
  std::vector< cv::Mat >::const_iterator m_it = masks.begin();
  cv::Mat mask;
  cv::Scalar mean, std_dev;
  unsigned int img_n = 0;
  for (;img_it!=images.end();++img_it,++m_it,++img_n) {
	m_it->convertTo(mask,CV_8U);
	cv::meanStdDev(*img_it,mean,std_dev,mask);

    if (std_dev[0]<1e-5) {
      std::string err_msg("Insufficient variance in image number "+img_n);
      throw(ProcessingException(err_msg.c_str()));
    }

    ms_vector.push_back(std::pair< float,float >(mean[0],std_dev[0]));
  }
}

void Utils::compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float,float > >& ms_vector,
								const unsigned int border_size) {
  std::vector< cv::Mat >::const_iterator img_it = images.begin();
  cv::Scalar mean, std_dev;
  unsigned int img_n = 0;

  for (;img_it!=images.end();++img_it,++img_n) {
	cv::Mat img_roi = (*img_it)(cv::Range(border_size,img_it->rows-border_size),cv::Range(border_size,img_it->cols-border_size));
	cv::meanStdDev(img_roi,mean,std_dev);
    if (std_dev[0]<1e-5) {
      std::string err_msg("Insufficient variance in image number "+img_n);
      throw(ProcessingException(err_msg.c_str()));
    }
    ms_vector.push_back(std::pair< float,float >(mean[0],std_dev[0]));
  }
}

void Utils::binarize_image(cv::Mat& img) {
  double min, max;
  cv::minMaxLoc(img,&min,&max);
  cv::threshold(img,img,(max-min)/2.0,1.0,cv::THRESH_BINARY);
}

void Utils::save_debug_mat(const cv::Mat& matrix,const char* out_filename) {
  std::cerr << "Writing debug file " << out_filename << std::endl;
  FILE *fp = fopen(out_filename,"wt");

  for (int r=0;r<matrix.rows;++r) {
	const float *Mr = matrix.ptr< float >(r);
	for (int c=0;c<matrix.cols;++c) {
	  if (fprintf(fp,"%f ",Mr[c])<0) {
		std::string err_msg("Unexpected end of file encountered while writing a filter bank to file "+std::string(out_filename));
		throw(FileException(err_msg.c_str()));
	  }
	}
	fprintf(fp,"\n");
  }
  fclose(fp);
}

cv::Mat Utils::convert_img_visualization_color(const cv::Mat& original_img) {
  cv::Mat color_img(original_img.rows,original_img.cols,CV_8UC3);

  // The three color planes that will be used to encode the error information
  cv::vector< cv::Mat > color_pl;
  cv::split(color_img,color_pl);

  // Residual values are in [-1, 1], therefore we'll take the absolute value
  const float min = 0;
  const float max = 1;

  // Fraction of the domain values
  const float fract = (max-min)/5;

  // Iterators over the considered matrices
  cv::MatConstIterator_< float > src_it = original_img.begin< float >();
  cv::MatIterator_< uchar > R_it = color_pl[2].begin< uchar >();
  cv::MatIterator_< uchar > G_it = color_pl[1].begin< uchar >();
  cv::MatIterator_< uchar > B_it = color_pl[0].begin< uchar >();

  for (;src_it!=original_img.end< float >();++src_it,++R_it,++G_it,++B_it) {
	float value= fabs(*src_it);

	if (value<fract)        { *R_it = 0; *G_it = 0; *B_it = 128+(unsigned char)(127*value/fract);                     }
	else if (value<2*fract) { *R_it = 0; *G_it = (unsigned char)(255*((value-fract)/fract)); *B_it = 255;             }
	else if (value<3*fract) { *R_it = (unsigned char)(255*((value-(2*fract))/fract)); *G_it = 255; *B_it = 255-*R_it; }
	else if (value<4*fract) { *R_it = 255; *G_it = (unsigned char)(255-255*((value-3*fract)/fract)); *B_it=0;         }
	else                    { *R_it = 255-(unsigned char)(127*((value-4*fract)/fract)); *G_it = 0; *B_it = 0;         }
  }

  // Merge back planes into an image
  cv::merge(color_pl,color_img);

  return(color_img);
}

cv::Mat Utils::convert_img_visualization(const cv::Mat& original_img) {
  cv::Mat final_img = original_img.clone();
  double min, max;

  // Normalize image (center it so that gray represents the 0)
  cv::minMaxLoc(final_img,&min,&max);
  float max_abs_value = fabs(min) > fabs(max) ? fabs(min) : fabs(max);

  if (max_abs_value<1e-5 || max==min)
    final_img = 127.5;
  else {
    final_img /= max_abs_value;
    final_img += 1;
    final_img *= 127.5;
  }

  final_img.convertTo(final_img,CV_8UC1,1,0);
  return(final_img);
}

cv::Mat Utils::magnify_matrix(const cv::Mat& src_mat,const unsigned int scale_factor) {
  cv::Mat dst_mat(src_mat.rows*scale_factor,src_mat.cols*scale_factor,CV_32FC1);

  for (int r=0;r<src_mat.rows;++r) {
	const float *Mr = src_mat.ptr< float >(r);
	for (int c=0;c<src_mat.cols;++c) {
	  cv::Mat roi = dst_mat(cv::Range(r*scale_factor,(r+1)*scale_factor),cv::Range(c*scale_factor,(c+1)*scale_factor));
	  roi.setTo(Mr[c]);
	}
  }

  return(dst_mat);
}

void Utils::normalize_image_set(std::vector< cv::Mat >& images,const std::vector< std::pair< float, float > >& ms_vector) {
  std::vector< cv::Mat >::iterator img_it = images.begin();
  std::vector< std::pair< float,float > >::const_iterator ms_it = ms_vector.begin();

  for (;img_it!=images.end();++img_it,++ms_it) {
    cv::Scalar_<float> mean(ms_it->first),std_dev(ms_it->second);
	*img_it -= mean[0];
	*img_it /= std_dev[0];
  }
}

void Utils::correlate_valid(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst) {
  cv::Mat tmp_dst;
  // incr deals with even-sized filters
  unsigned int incr=0;
  if (kernel.rows%2==0) {
	incr=1;
  }
  cv::filter2D(src, tmp_dst,-1,kernel);
  dst = tmp_dst(cv::Range(kernel.rows/2,src.cols-kernel.rows/2+incr),cv::Range(kernel.rows/2,src.cols-kernel.rows/2+incr));
}

void Utils::convolve_valid(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst) {
  cv::Mat tmp_dst;
  cv::Mat flipped_kernel(kernel.rows,kernel.cols,CV_32FC1);
  cv::flip(kernel,flipped_kernel,-1);
  // incr deals with even-sized filters
  unsigned int incr=0;
  if (kernel.rows%2==0) {
	incr=1;
  }
  cv::filter2D(src,tmp_dst,-1,flipped_kernel);
  dst = tmp_dst(cv::Range(kernel.rows/2,src.cols-kernel.rows/2+incr),cv::Range(kernel.rows/2,src.cols-kernel.rows/2+incr));
}

void Utils::convolve_full(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst) {
  cv::Mat flipped_kernel(kernel.rows,kernel.cols,CV_32FC1);
  cv::flip(kernel,flipped_kernel,-1);

  cv::Mat framed_src(src.rows+2*(kernel.rows-1),2*(src.rows+kernel.rows-1),CV_32FC1);
  // Enlarge more and then take the valid correlation because in this way we can use border reflection
  // (I don't trust OpenCV enough to let it do the job)
  cv::copyMakeBorder(src,framed_src,kernel.rows-1,kernel.rows-1,kernel.cols-1,kernel.cols-1,cv::BORDER_REFLECT101);

  correlate_valid(framed_src,flipped_kernel,dst);
}
