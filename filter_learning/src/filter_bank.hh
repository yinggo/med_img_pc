/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef FILTER_BANK_HH
#define FILTER_BANK_HH

#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include "opencv2/opencv.hpp"

#include "dataset.hh"
#include "parameters.hh"
#include "exceptions.hh"
#include "utils.hh"

#define DATASET_TYPE_STRLEN    256

class Filter_bank {
public:
  Filter_bank() { };
  virtual ~Filter_bank() { };

  // Iterators over the internal filters (constant access only!)
  typedef std::vector< cv::Mat >::const_iterator const_iterator;
  virtual const_iterator begin() const = 0;
  virtual const_iterator end() const = 0;
  virtual const_iterator element_at(const unsigned int el_number) const = 0;

  virtual void update_filters(const std::vector< cv::Mat >& gradients) = 0;
  virtual void store_filter_bank(const unsigned int iteration_number) const = 0;
  virtual cv::Mat get_filter_bank_representation() const = 0;

  virtual void get_fb_dimensions(struct fb_dimensions& dims) const = 0;

protected:
  virtual void parse_filter_bank_config(const std::string& filter_bank_config_filename) = 0;

  mutable cv::RNG m_rng;
};

struct fb_dimensions {
  unsigned int filters_no;
  unsigned int dims_no;
  std::vector< unsigned int > dims;
};

#endif // FILTER_BANK_HH
