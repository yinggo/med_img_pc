/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef SPARSEIT_2D_SS_HH
#define SPARSEIT_2D_SS_HH

#include <iostream>
#include <string>
#include "opencv2/opencv.hpp"
#include <boost/shared_ptr.hpp>
#include <omp.h>
#include <csignal>

#include "../cmd_line.hh"
#include "../filter_bank.hh"
#include "../dataset.hh"
#include "../optimization.hh"
#include "../parameters.hh"
#include "../exceptions.hh"
#include "../utils.hh"

extern sig_atomic_t terminating_signal;

class SparseIT_2D_SS : public Optimization {
public:
  SparseIT_2D_SS(CmdLine& cmd_line,Parameters& config,boost::shared_ptr< Filter_bank >& filters,boost::shared_ptr< Dataset >& dataset);
  virtual ~SparseIT_2D_SS() { };

  virtual void optimize_filters();

protected:
  SparseIT_2D_SS() { }; // Callable only from derived classes
  virtual void parse_optimization_config(const std::string& opt_algo_config_filename);

  void initialize_feature_maps(const cv::Mat& sample);
  cv::Mat compute_reconstruction();
  cv::Mat compute_restricted_reconstruction() const;

  void compute_feature_maps(const cv::Mat& sample);
  void compute_filters_gradients(const cv::Mat& sample,std::vector< cv::Mat >& filters_gradient) const;

  void reset_filter_gradients(std::vector< cv::Mat >& filters_gradient);

  void ISTA(cv::Mat& src,const cv::Mat& gradient,const float GD_step,const float reg_param) const;

  unsigned int m_iteration_number;
  std::string m_results_directory;

  float m_eta_coeffs;
  float m_eta_filters;
  float m_lambda_learn;
  unsigned int m_coeffs_n_it;
  unsigned int m_filters_n_it;
  bool m_penalize_similar_filters;
  float m_xi_filters;

  boost::shared_ptr< Filter_bank > m_filters;
  boost::shared_ptr< Dataset > m_dataset;

  unsigned int m_filters_no;
  unsigned int m_filters_size;
  unsigned int m_sample_size;
  unsigned int m_extended_sample_size;
  unsigned int m_fm_size;
  unsigned int m_rec_size;

  std::vector< cv::Mat > m_feature_maps;
  std::vector< cv::Mat > m_test_set;
};

#endif // SPARSEIT_2D_SS_HH
