/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "dataset_SS.hh"

Dataset_2D_SS::Dataset_2D_SS(Parameters& config,boost::shared_ptr< Filter_bank >& filters,cv::RNG& rng) {
  parse_dataset_config(config.get_dataset_config_filename());

  // Get system's random number generator
  m_rng = rng;

  // Set enlarged patch size according to filter size
  struct fb_dimensions fb_dims;
  filters->get_fb_dimensions(fb_dims);
  m_enlarged_sample_size = m_sample_size+2*fb_dims.dims.at(0);

  // Check if the image (and, eventually, the mask) file list is present
  Utils::file_exists(m_images_list_filename);
  if (m_use_masks)
	Utils::file_exists(m_masks_list_filename);

  std::cerr << "  Loading images" << std::endl;
  load_data(m_images,m_images_list_filename);
  m_images_no = m_images.size();
  std::cerr << "    Loaded " << m_images_no << " images" << std::endl;
  if (m_use_masks) {
	std::cerr << "  Loading masks" << std::endl;
    load_data(m_masks,m_masks_list_filename,m_binarize_masks);
	unsigned int masks_no = m_masks.size();
	if (m_images_no!=masks_no) {
	  std::string err_msg("The number of images is different from the number of masks");
	  throw(DatasetException(err_msg.c_str()));
	}
  }
  else {
	// Create fake masks to ease coding
	std::cerr << "  Creating fake masks" << std::endl;
	create_empty_masks();
  }

  if (!m_sample_based_normalization) {
	std::cerr << "  Performing image-based normalization" << std::endl;
	std::vector< std::pair< float,float > > ms_vector;
	if (m_use_masks)
	  Utils::compute_mean_stddev(m_images,ms_vector,m_masks);
	else
	  Utils::compute_mean_stddev(m_images,ms_vector);
	Utils::normalize_image_set(m_images,ms_vector);
  }
}

void Dataset_2D_SS::load_data(std::vector< cv::Mat >& dst_vector,const std::string& data_list_filename,const bool binarize_flag) {
  std::vector< std::string > filenames;
  dst_vector.clear();

  std::ifstream input_file(data_list_filename.c_str());
  std::istream_iterator< std::string > begin(input_file);
  std::istream_iterator< std::string > end;
  while (begin!=end) {
    filenames.push_back(*begin++);
  }
  std::sort(filenames.begin(),filenames.end());

  for (std::vector< std::string >::const_iterator filename_it=filenames.begin();filename_it!=filenames.end();++filename_it) {
    cv::Mat tmp_mat = cv::imread(*filename_it,0);
    tmp_mat.convertTo(tmp_mat,CV_32FC1,1./(float)255,0);

    if (binarize_flag)
      Utils::binarize_image(tmp_mat);

    dst_vector.push_back(tmp_mat);
  }

  if (dst_vector.size()==0) {
    std::string err_msg("No images loaded ");
    throw(DatasetException(err_msg.c_str()));
  }
}

void Dataset_2D_SS::parse_dataset_config(const std::string& dataset_config_filename) {
  po::options_description cfg_file_descr("Dataset configuration file parameters");
  cfg_file_descr.add_options()
	("dataset_name", po::value< std::string >(&(m_dataset_name)),
	 "Dataset's name")
	("images_list_filename", po::value< std::string >(&(m_images_list_filename)),
	 "Name of the file containing the list of input images")
	("use_masks", po::value< bool >(&(m_use_masks)),
	 "Flag for enabling the use of masks")
	("binarize_masks", po::value< bool >(&(m_binarize_masks)),
	 "Image masks require binarization")
	("masks_list_filename", po::value< std::string >(&(m_masks_list_filename))->default_value(""),
	 "Name of the file containing the list of input masks")
	("sample_based_normalization", po::value< bool >(&(m_sample_based_normalization)),
	 "Enable sample-based (instead of image-based) normalization")
	("sample_size", po::value< unsigned int >(&(m_sample_size)),
	 "Sample size")
	("sample_min_stddev", po::value< float >(&(m_sample_min_stddev)),
	 "Minimum standard deviation for a sample to be accepted");

  // Parse config file
  std::ifstream ifs(dataset_config_filename.c_str());
  if (!ifs.is_open()) {
	throw ConfigParseException("Unable to read the dataset configuration file");
  }
  po::variables_map cfg_file_vm;
  po::store(po::parse_config_file(ifs,cfg_file_descr),cfg_file_vm);
  po::notify(cfg_file_vm);
  ifs.close();

  // Validate optional parameters
  if (m_use_masks && m_masks_list_filename=="")
	throw ConfigParseException("Masks are requested, but no mask list specified");
}

void Dataset_2D_SS::create_empty_masks() {
  for (std::vector< cv::Mat >::const_iterator img_it=m_images.begin();img_it!=m_images.end();++img_it) {
	cv::Mat empty_mask(img_it->rows,img_it->cols,CV_32FC1,cv::Scalar_<float>(1));
	m_masks.push_back(empty_mask);
  }
}

cv::Mat Dataset_2D_SS::get_sample() const {
  cv::Mat rnd_image;
  cv::Mat rnd_mask;
  cv::Mat image_sample;
  double min_mask, max_mask;
  cv::Scalar sample_mean, sample_std;

  // Set the sample size to the enlarged sample size
  const unsigned int sample_size = m_enlarged_sample_size;

  while (true) {
	unsigned int img_n = m_rng(m_images_no);
	rnd_image = m_images.at(img_n);
	rnd_mask = m_masks.at(img_n);

	for (unsigned int i_choice=0;i_choice<MAX_ATTEMPTS_IMG;++i_choice) {
	  unsigned int row_n = m_rng(rnd_image.rows-sample_size);
	  unsigned int col_n = m_rng(rnd_image.cols-sample_size);
	  cv::minMaxLoc(rnd_mask(cv::Range(row_n,row_n+sample_size),cv::Range(col_n,col_n+sample_size)),&min_mask,&max_mask);

	  if (min_mask>0) {
		cv::Mat tmp_ref = rnd_image(cv::Range(row_n,row_n+sample_size),cv::Range(col_n,col_n+sample_size));
		tmp_ref.copyTo(image_sample);

		cv::meanStdDev(image_sample,sample_mean,sample_std);

		if (sample_std[0]>=m_sample_min_stddev) {
		  if (m_sample_based_normalization) {
			image_sample -= sample_mean[0];
			image_sample /= sample_std[0];
		  }

		  return(image_sample);
		}
	  }
	}
  }
}
