% Code and description of the algorithm from John Wright and Allen Y. Yang,
% University of California, Berkeley

% log2_quotient 
%   helper function for computing the mutual information
%   returns a matrix whose ij entry is
%     log2( a_ij / b_ij )   if a_ij, b_ij > 0
%     0                     if a_ij is 0
%     log2( a_ij + 1 )      if a_ij > 0 but b_ij = 0 (this behavior should
%                                         not be encountered in practice!
function [lq] = log2_quotient(A,B)

lq = log2((A+((A == 0).*B)+(B == 0)) ./ (B+(B == 0)));

end
