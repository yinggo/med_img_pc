function [] = compute_VI(analytical_dir_rep,response,gt,test_img_name,best_threshold)
%  compute_VI  Compute the Variation of Information measure for the given
%              segmentation
%
%  Algorithm taken from the code published by Allen Y. Yang
%  http://www.eecs.berkeley.edu/~yang/software/lossy_segmentation/
%
%  Synopsis:
%     compute_VI(analytical_dir_rep,response,gt,test_img_name,best_threshold)
%
%  Input:
%     analytical_dir_rep = directory where the VI measure for the given
%                          segmentation will be saved
%     response           = response file given by the classifier
%     gt                 = ground-truth image
%     test_img_name      = name of the test image
%     best_threshold     = threshold for which the highest F-measure has been
%                          attained

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

% Threshold response at best threshold
response(response<best_threshold) = 0;
response(response>=best_threshold) = 1;
count_matrix = compute_count_matrix(response,gt);

N = sum(count_matrix(:));
% Joint probability mass function of the two labels
joint = count_matrix./N;
marginal2 = sum(joint,1); % Sum over rows
marginal1 = sum(joint,2); % Sum over cols

H1 = -sum(marginal1.*log2(marginal1+(marginal1==0))); % Entropy of the first label
H2 = -sum(marginal2.*log2(marginal2+(marginal2==0))); % Entropy of the second label
MI = sum(sum(joint.*log2_quotient(joint,marginal1*marginal2))); % Mutual information

VI_score = H1+H2-2*MI; 
    
VI_filename = fullfile(analytical_dir_rep,sprintf('VI_%s.txt',test_img_name));
fd = fopen(VI_filename,'wt');
fprintf(fd,'%f\n',VI_score);
fclose(fd);

end
