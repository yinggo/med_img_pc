function [] = test_RF(results_path,model,test_imgs_list,test_filenames,i_rep)
%  test_RF  Test a trained Random Forest classifier on the given test data
%
%  Synopsis:
%     test_RF(results_path,model,test_imgs_list,test_filenames,i_rep)
%
%  Input:
%     results_path   = path for storing the segmented images
%     model          = trained Random Forest classifier
%     test_imgs_list = list of file names for the test images (used to generate
%                      the filename of the segmented images)
%     test_filenames = list of files containing the feature maps in a format
%                      suitable for the classifier
%     i_rep          = iteration number (for multiple random tests)

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fprintf('  Testing Random Forest classifier on input data...\n');

[status,message,messageid] = mkdir(fullfile(results_path,sprintf('rep_%03d',i_rep))); %#ok<ASGLU>

parfor i_test_file = 1:length(test_filenames)
    [test_img_path,test_img_name,test_img_ext] = fileparts(test_imgs_list{i_test_file}); %#ok<*NASGU,ASGLU>
    test_data = load(test_filenames{i_test_file});
    
    % The last column contains the test labels
    features = test_data(:,1:end-1);
    
    votes = vigraPredictProbabilitiesRF(model,features);
    
    % Dump the result in NRRD format
    result_filename = fullfile(results_path,sprintf('rep_%03d',i_rep),sprintf('%s.nrrd',test_img_name));
    % Get the size of the original image (the classifier dumps the prediction as
    % a single column) and reshape the predictions
    test_img = imread(test_imgs_list{i_test_file});
    nrows = size(test_img,1);
    ncols = size(test_img,2);
    output = zeros(nrows,ncols);
    for i_row = 1:nrows
        for i_col = 1:ncols
            output(i_row,i_col) = votes((i_row-1)*ncols+i_col,2);
        end
    end
    nrrdSave(result_filename,output');
end

end
