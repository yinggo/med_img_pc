function [] = compute_ROC(ROC_dir_rep,response,gt,test_img_name,thresholds_no,fpr_eval_grid)
%  compute_ROC  Compute Receiver Operating Characteristic curve for the given
%               image
%
%  Synopsis:
%     compute_ROC(ROC_dir_rep,response,gt,test_img_name,thresholds_no,fpr_eval_grid)
%
%  Input:
%     ROC_dir_rep   = directory where the ROC curve for the given image will be
%                     saved
%     response      = response file given by the classifier
%     gt            = ground-truth image
%     test_img_name = name of the test image
%     thresholds_no = number of thresholds used in ROC computation
%     fpr_eval_grid = grid over which the ROC will be computed

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

gt(gt==0) = -1;

% Find the count of positive and negative samples
n_pos = size(find(gt==1),1);
n_neg = size(find(gt==-1),1);

% First and last elements are fixed (0 and 1)
n_steps = thresholds_no-2;

dt = [response,gt];
dto = sortrows(dt);
dtc = dto(:,2);

res = zeros(thresholds_no,2);
res(1,:) = [1,1];

step_size = floor(size(response,1)/n_steps);

for i = 1:n_steps
    % Vector used to calculate the fpr and the tpr
    vals = [zeros(1,i*step_size),ones(1,size(response,1)-i*step_size)]';
    % We multiply the vector by the labels orders
    labl = dtc.*vals;
    %% The true positives are the values in which the vector is one
    TPR  = size(find(labl==1),1)/n_pos;
    FPR  = size(find(labl==-1),1)/n_neg;
    %% We concatenate it to the res vector
    res(i+1,:) = [TPR,FPR];
end

res(end,:) = [0,0];
tpr = res(:,1);
fpr = res(:,2);

% Perform spline interpolation
tpr_fit = csapi(fpr,tpr);
tpr = fnval(tpr_fit,fpr_eval_grid);
tpr(tpr>1) = 1;
tpr(tpr<0) = 0;

% Compute Area Under Curve by integration
auc = -trapz(fpr_eval_grid(end:-1:1),tpr(end:-1:1));

ROC_filename = fullfile(ROC_dir_rep,sprintf('ROC_%s.txt',test_img_name));
AUC_filename = fullfile(ROC_dir_rep,sprintf('AUC_%s.txt',test_img_name));

fd = fopen(ROC_filename,'wt');
for i_pt = 1:thresholds_no
    fprintf(fd,'%f %f\n',fpr_eval_grid(i_pt),tpr(i_pt));
end
fclose(fd);

fd = fopen(AUC_filename,'wt');
fprintf(fd,'%f\n',auc);
fclose(fd);

end
