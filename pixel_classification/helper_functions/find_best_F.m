function [bestP,bestR,bestF,bestT] = find_best_F(precision,recall,thresholds)
%  find_best_F  Find the highest F-measure value for the given PR curve
%
%  Synopsis:
%     [bestP,bestR,bestF,bestT] = find_best_F(precision,recall,thresholds)
%
%  Input:
%     precision  = precision values
%     recall     = recall values
%     thresholds = set of thresholds used in PR computation
%  Output:
%     bestP = precision value at which the highest F-measure occurs
%     bestR = recall value at which the highest F-measure occurs
%     bestF = highest F-measure found
%     bestT = threshold value at which the highest F-measure occurs

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

[bestP,bestR,bestF,bestT] = deal(precision(1),recall(1),compute_F_measure(precision(1),recall(1)),thresholds(1));

for iThreshold = 2:length(thresholds)
    r = recall(iThreshold);
    p = precision(iThreshold);
    f = compute_F_measure(p,r);
    if(f > bestF)
        [bestP,bestR,bestF,bestT] = deal(p,r,f,thresholds(iThreshold));
    end
end

end
