function [] = train_l1reg(p,train_data_filename,train_labels_filename,i_rep)
%  train_l1reg  Train a l1-penalized logistic regression classifier on the given
%               training data 
%
%  Synopsis:
%     train_l1reg(p,train_data_filename,train_labels_filename,i_rep)
%
%  Input:
%     p                     = structure containing system's configuration and
%                             paths 
%     train_data_filename   = name of the file containing the training data
%     train_labels_filename = name of the file containing the training labels
%     i_rep                 = iteration number (for multiple random tests)

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fprintf('  Training l1-penalized logistic regressor...\n');

model_filename = fullfile(p.paths.var,sprintf('l1_reg_model_rep%03d',i_rep));

if (~exist(p.l1_train_path,'file'))
    error('Wrong path for the l1 regressor (%s)',p.l1_train_path);
end

system(sprintf('%s -s %s %s %f %s',p.l1_train_path,train_data_filename,train_labels_filename,p.l1reg.lambda,model_filename));

end
