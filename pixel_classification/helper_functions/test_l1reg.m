function [] = test_l1reg(p,test_filenames,i_rep)
%  test_l1reg  Test a trained l1-penalized regressor on the given test data
%
%  Synopsis:
%     test_l1reg(results_path,model,test_imgs_list,test_filenames,i_rep)
%
%  Input:
%     p                  = structure containing system's configuration and paths 
%     test_data_filename = name of the file containing the test data
%     i_rep              = iteration number (for multiple random tests)

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fprintf('  Testing l1-penalized logistic regressor on input data...\n');

results_path = p.paths.responses;
test_imgs_list = p.test_imgs_list;
model_filename = fullfile(p.paths.var,sprintf('l1_reg_model_rep%03d',i_rep));

if (~exist(p.l1_test_path,'file'))
    error('Wrong path for the l1 classifier (%s)',p.l1_test_path);
end

[status,message,messageid] = mkdir(fullfile(results_path,sprintf('rep_%03d',i_rep))); %#ok<ASGLU>
[status,message,messageid] = mkdir(fullfile(p.paths.res,'predictions',sprintf('rep_%03d',i_rep))); %#ok<ASGLU>

for i_test_file = 1:length(test_filenames)
    [test_img_path,test_img_name,test_img_ext] = fileparts(test_imgs_list{i_test_file}); %#ok<*NASGU,ASGLU>
    
    pred_filename = fullfile(p.paths.res,'predictions',sprintf('rep_%03d',i_rep),sprintf('%s',test_img_name));
    system(sprintf('%s -p %s %s %s',p.l1_test_path,model_filename,test_filenames{i_test_file},pred_filename));
    
    pred = mmread(pred_filename);
    
    result_filename = fullfile(results_path,sprintf('rep_%03d',i_rep),sprintf('%s.nrrd',test_img_name));
    test_img = imread(test_imgs_list{i_test_file});
    
    % Reshape prediction to the same format of input image
    nrows = size(test_img,1);
    ncols = size(test_img,2);
    output = zeros(nrows,ncols);
    for i_row = 1:nrows
        for i_col = 1:ncols
            output(i_row,i_col) = pred((i_row-1)*ncols+i_col);
        end
    end
    nrrdSave(result_filename,output');
end

end
